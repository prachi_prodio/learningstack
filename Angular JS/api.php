<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
header("access-control-allow-origin: *");
header('content-type: application/json; charset=utf-8');
include_once("../include/config.php");
include_once(PATH."startup.php");
include_once(CLASS_PATH."http.class.php");
include_once(CLASS_PATH."celebzone.class.php");
$link = new mysql();
$returnArr = array();
$celebzone = new Celebzone();

function getCelebData($page='',$limit='',$conditionArray){
	global $link;
	$categoryQry = '';$returnArr = array();
	$page = $page!=''?$page:0;
	$limit = $limit!=''?$limit:50;
	$startlimit = $page * $limit;
	$endlimit = $limit;
	
	$limitQry = ' limit '.$startlimit.','.$endlimit;
	$orderQry = ' order by tcd.celeb_id desc';
	if(isset($conditionArray['categoryid']) && $conditionArray['categoryid']>0){
		$categoryQry .=' and tcd.celeb_category = "'.$conditionArray['categoryid'].'"';
	}
	if(isset($conditionArray['celeb_id']) && $conditionArray['celeb_id']>0){
		$categoryQry .=' and tcd.celeb_id = "'.$conditionArray['celeb_id'].'"';
	}
	
	$test = "select * from tbl_celeb_data tcd where tcd.status = '1' ".$categoryQry."".$orderQry."".$limitQry;
	$result = mysqli_query($link,$test);
	$loop = 0;
	while($row = $result->fetch_assoc()){
		$returnArr[$loop]['celeb_id'] = $row['celeb_id'];
		$returnArr[$loop]['celeb_name'] = $row['celeb_name'];
		$returnArr[$loop]['celeb_image_ls_l'] = http::getImage($row['celeb_image'],'ls_l');
		$returnArr[$loop]['celeb_image_ls_m'] = http::getImage($row['celeb_image'],'ls_m');
		$returnArr[$loop]['celeb_image_t'] = http::getImage($row['celeb_image'],'t');
		$returnArr[$loop]['celeb_thumbnail_ls_l'] = http::getImage($row['celeb_thumbnail'],'ls_l');
		$returnArr[$loop]['celeb_thumbnail_ls_m'] = http::getImage($row['celeb_thumbnail'],'ls_m');
		$returnArr[$loop]['celeb_thumbnail_t'] = http::getImage($row['celeb_thumbnail'],'t');
		$returnArr[$loop]['celeb_category_id'] = $row['celeb_category'];
		$loop++;
	}
	return $returnArr;
}
function getCelebSocialData($page='',$limit='',$conditionArray){
	global $link,$celebzone;
	$celebQry = '';$returnArr = array();
	$page = $page!=''?$page:0;
	$limit = $limit!=''?$limit:50;
	$startlimit = $page * $limit;
	$endlimit = $limit;

	$limitQry = ' limit '.$startlimit.','.$endlimit;
	$orderQry = ' order by tcsd.media_time desc';
	if(isset($conditionArray['celeb_id']) && $conditionArray['celeb_id']>0){
		$celebQry .=' and tcsd.celeb_id = "'.$conditionArray['celeb_id'].'"';
	}
	$qry = "select tcd.celeb_id,tcsd.social_id,tcsd.social_text,tcsd.social_url,tcsd.media_url,tcsd.media_type,tcsd.socialmedia,tcsd.media_time,
			tcd.celeb_name,tcd.celeb_image,tcd.celeb_thumbnail from tbl_celeb_social_data tcsd 
			join tbl_celeb_data tcd on tcd.celeb_id=tcsd.celeb_id where 1=1 and tcd.sitename='celebzone' ".$celebQry."".$orderQry."".$limitQry;
	//echo $test;die();
	$result1 = mysqli_query($link,$qry);
	$loop = 0;
	while($row = $result1->fetch_assoc()){
		$returnArr[$loop]['celeb_id']=$row['celeb_id'];
		$returnArr[$loop]['social_id']=$row['social_id'];
		$returnArr[$loop]['social_text']=$row['social_text'];
		$returnArr[$loop]['social_url']=$row['social_url'];
		$returnArr[$loop]['media_url']=$row['media_url'];
		$returnArr[$loop]['media_type']=$row['media_type'];
		$returnArr[$loop]['socialmedia']=$row['socialmedia'];
		$returnArr[$loop]['media_time']= $celebzone->getTime($row['celeb_id']);
		$returnArr[$loop]['celeb_name']=$row['celeb_name'];
		$returnArr[$loop]['celeb_image_ls_l'] = http::getImage($row['celeb_image'],'ls_l');
		$returnArr[$loop]['celeb_image_ls_m'] = http::getImage($row['celeb_image'],'ls_m');
		$returnArr[$loop]['celeb_image_t'] = http::getImage($row['celeb_image'],'t');
		$returnArr[$loop]['celeb_thumbnail_ls_l'] = http::getImage($row['celeb_thumbnail'],'ls_l');
		$returnArr[$loop]['celeb_thumbnail_ls_m'] = http::getImage($row['celeb_thumbnail'],'ls_m');
		$returnArr[$loop]['celeb_thumbnail_t'] = http::getImage($row['celeb_thumbnail'],'t');
		$loop++;
	}
	return $returnArr;
}

$possible_url = array("getcelebdata", "getsocialdata");

$value = array(0=>"An unexpected error has occurred");

if (isset($_GET["action"]) && in_array($_GET["action"], $possible_url))
{
$page = isset($_GET['page'])?$_GET['page']:'';
$limit = isset($_GET['limit'])?$_GET['limit']:'';
  switch ($_GET["action"])
    {
      case "getcelebdata":
        $value = getCelebData($page,$limit,$_GET);
        break;
      case "getsocialdata":
          $value = getCelebSocialData($page,$limit,$_GET);
		  break;
	  default:
          $value = array(0=>"Missing argument");
          break;
    }
}else{
	$value = array(0=>"Invalid action parameter");
}
//echo"<pre>";print_r($value);die();
echo json_encode($value);die();
?>