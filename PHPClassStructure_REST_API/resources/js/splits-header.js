var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();

googletag.cmd.push(function() {
	googletag.defineSlot('/4687/mtvindia.com/in_splitsvilla', [728, 90], 'div-gpt-ad-1436163106041-0').addService(googletag.pubads());
	googletag.defineSlot('/4687/mtvindia.com/in_splitsvillarhs', [300, 250], 'div-gpt-ad-1436163106041-1').addService(googletag.pubads());
	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
});